# Hayase (Nagabot)
[![Discord Bots](https://top.gg/api/widget/status/699675018644160665.svg)](https://top.gg/bot/699675018644160665)
[![Discord Bots](https://top.gg/api/widget/servers/699675018644160665.svg)](https://top.gg/bot/699675018644160665)

[![](https://img.shields.io/static/v1?label=language&message=JavaScript&color=blue&style=flat&logo=javascript)](https://en.wikipedia.org/wiki/JavaScript)
[![npm](https://img.shields.io/npm/v/node?label=node.js&logo=node.js&style=flat)](https://nodejs.org/en/)
[![npm](https://img.shields.io/npm/v/discord.js?label=discord.js&logo=discord&style=flat)](https://discord.js.org/?source=post_page---------------------------#/)
[![license: GPLv3](https://img.shields.io/static/v1?label=license&message=GPLv3&color=blue&style=flat&logo=gnu)](https://choosealicense.com/licenses/gpl-3.0/)
[![Gitter](https://badges.gitter.im/hayase-discordbot/dev.svg)](https://gitter.im/hayase-discordbot/dev?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

**Hayase is a Discord bot written in JavaScript using the discord.js library.** 

I'm based on the character Hayase Nagatoro from the manga/anime “Please don't bully me miss Nagatoro“

**Commands:**
*  Reactions / Emotions: `/happy`, `/smug`, `/sup`, `/smirk`, `/smile`, `/giggle`, `/wink`, `/gross`, `/thumbsup`, `/thup`, `/bye`, `/duel`
*  Utility: `/help`, `/boop`, `/changelog`, `/commits`, `/contact`

## Development

* Fork the repository.

* Run `yarn` or `npm install` to install all dependencies. @top-gg is required for the client to function but can be removed as it is not necessary for the functionality of the client.

* Remove this line in `index.js` IF you remove the @top-gg dependency

```
...

// TopGG stats updater
// Optional dependency, can be removed
const { AutoPoster } = require('topgg-autoposter');

const ap = AutoPoster(process.env.TOPGG_TOKEN, client);

ap.on('posted', () => {
	console.log('[STAT] Posted client stats to Top.gg');
});

...

```

* Fill in the required .env variables like `CLIENTID=` and `DISCORD_TOKEN=`

* Run `node ./deploy-slash.js` to register the clients slash commands, then do `node .` to run the client.


## Support
Your donations go into the upkeep for the bot or a good coffee to keep myself working. ❤

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V026P34)

## License

This project uses the GNU General Public License v3.0
