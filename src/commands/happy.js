const { SlashCommandBuilder } = require('@discordjs/builders');
const { happyarray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('happy')
		.setDescription('Show off ur positivity.'),
	async execute(interaction) {
		interaction.reply(`${interaction.member} is happy. \n${happyarray[Math.floor(Math.random() * happyarray.length)]}`);
	},
};