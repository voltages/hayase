const { SlashCommandBuilder } = require('@discordjs/builders');
const { smugarray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('smug')
		.setDescription('Be smug at senpai.')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to smug at.')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} giving smug look at ${user}\n ${smugarray[Math.floor(Math.random() * smugarray.length)]}`);
		interaction.reply(`${interaction.member} is looking smug. \n${smugarray[Math.floor(Math.random() * smugarray.length)]}`);
	},
};