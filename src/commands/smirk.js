const { SlashCommandBuilder } = require('@discordjs/builders');
const { smirkarray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('smirk')
		.setDescription('Smirk at people.')
		.addUserOption(option => option.setName('mention').setDescription('Select a user to smirk at.')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} is smirking at ${user}\n ${smirkarray[Math.floor(Math.random() * smirkarray.length)]}`);
		interaction.reply(`${interaction.member} smirks... \n${smirkarray[Math.floor(Math.random() * smirkarray.length)]}`);
	},
};