// Requires the Discord.js to be downloaded.
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('help')
		.setDescription('List all commands without scrolling through all of them.'),
	async execute(interaction) {
		const help = new MessageEmbed()
			.setColor('#b9b9b9')
			.setTitle('All commands currently registered into Hayase.')
			.addFields(
				{ name: 'Reactions / Emotions ', value: '`/happy` `/smug` `/sup` `/smirk` `/smile` `/giggle` `/wink` `/gross` `/thumbsup` `/thup` `/bye` `/duel`' },
				{ name: 'Utility', value: '`/help` `/boop` `/changelog` `/commits` `/contact`' },
			);
		return interaction.reply({ embeds: [help], ephemeral: true });
	},
};
